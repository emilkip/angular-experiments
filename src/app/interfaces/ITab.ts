export interface ITab {
  name: string;
  content: string;
  id: string;
}