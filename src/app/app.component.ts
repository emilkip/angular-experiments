import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnInit {
  tabs = [];
  selectedTab = undefined;
  formIsVisible = false;

  constructor() {}

  ngOnInit() {
    if (this.tabs.length) {
      this.selectedTab = this.tabs[0];
    }
  }

  selectTab(tab = {}) {
    this.selectedTab = tab;
  }

  onRemove(tabId) {
    this.tabs = this.tabs.filter((tab) => tab.id !== tabId);
    this.selectedTab = this.tabs.length ? this.tabs[0] : undefined;
  }

  onCreate(newTab) {
    this.tabs.push(newTab);
    if (!this.selectedTab) {
      this.selectedTab = newTab;
    }
  }

  toggleForm() {
    this.formIsVisible = !this.formIsVisible;
  }
}
