import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NewTabComponent } from './components/new-tab/new-tab.component';
import { TabContentComponent } from './components/tab/tab-content.component';
import { TabService } from './services/tab.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    TabService
  ],
  declarations: [
    AppComponent,
    NewTabComponent,
    TabContentComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
