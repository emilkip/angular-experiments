import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ITab } from '../../interfaces/ITab';

@Component({
  selector: 'tab-content',
  templateUrl: './tab-content.component.html',
  styleUrls: [ './tab.scss' ]
})
export class TabContentComponent {

  @Input() tab: ITab;
  @Output() onRemove = new EventEmitter();

  public removeTab() {
    this.onRemove.emit(this.tab.id);
  }
}
