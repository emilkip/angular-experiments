import { Component, Output, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ITab } from '../../interfaces/ITab';


@Component({
  selector: 'new-tab',
  templateUrl: './new-tab.component.html',
  styleUrls: ['./new-tab.component.scss']
})
export class NewTabComponent implements OnInit {
  newTabForm: FormGroup;
  name = new FormControl();
  content = new FormControl();

  @Output() onCreate = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {
    this.newTabForm = this.formBuilder.group({
      name: '',
      content: ''
    });
  }

  ngOnInit() {

  }

  public addTab() {
    const newTab: ITab = {
      name: this.name.value || '',
      content: this.content.value || '',
      id: Math.random().toString(36).substr(2, 9)
    };
    this.onCreate.emit(newTab);
    this.name.reset();
    this.content.reset();
  }
}
